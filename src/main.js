import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App.vue";
import VueRouter from "vue-router";
import Login from "./components/login.vue";
import Body from "./components/body.vue";
// import Login from "./components/login.vue";
// import User from "./components/user.vue";
// import Jokes from "./components/jokes.vue";

import { store } from "./store";

Vue.use(VueRouter);

const routes = [
  { path: "/", component: Login },
  { path: "/login", component: Login },
  {
    path: "/body",
    component: Body,
    beforeEnter: (to, from, next) => {
      if (store.state.authenticataion == false) {
        console.log("false");
        next("/login");
      } else {
        next();
      }
    },
  },
  // { path: "/user/:id", component: User },
];
const router = new VueRouter({ routes });
Vue.config.productionTip = false;
Vue.filter("Ucase", function(val) {
  return val.toUpperCase();
});

new Vue({
  router: router,
  store,
  render: (h) => h(App),
}).$mount("#app");
