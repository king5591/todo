import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    person: null,
    editIndex: undefined,
    authenticataion: false,
    userRegister: false,
    admin: { username: "admin@test.com", password: "admin123" },
    nameList: [
      {
        username: "amit@email.com",
        password: "amit123",
        id: 0,
        name: "Amit",
        tasks: [],
        completed: 0,
      },
      {
        username: "somu@email.com",
        password: "somu123",
        id: 1,
        name: "Somu",
        tasks: [],
        completed: 0,
      },
      {
        username: "jack@email.com",
        password: "jack123",
        id: 2,
        name: "Jack",
        tasks: [],
        completed: 0,
      },
      {
        username: "jill@email.com",
        password: "jill123",
        id: 3,
        name: "Jill",
        tasks: [],
        completed: 0,
      },
    ],
  },
  mutations: {
    setAuthentication(state, status) {
      state.authenticataion = status;
    },
    userRegistered(state, status) {
      state.userRegister = status;
    },
    setPerson: (state, index) => (state.person = state.nameList[index]),
    addPerson: (state, obj) => {
      state.userRegister = false;
      state.nameList.push(obj);
    },
    more: (state, index) => (state.person.tasks[index].readIndex = 1),
    less: (state, index) => (state.person.tasks[index].readIndex = -1),
    done: (state) => state.person.completed++,
    notDone: (state) => state.person.completed--,
    taskAssigned: (state, obj) => state.person.tasks.push(obj),
    taskDeleted: (state, index) => state.person.tasks.splice(index, 1),
    editTask: (state, index) => (state.editIndex = index),
    updateTask: (state, task) =>
      (state.person.tasks[state.editIndex].task = task),
  },
  actions: {
    async choosePerson(state, index) {
      state.commit("setPerson", index);
    },
    async newPerson(state, obj) {
      state.commit("addPerson", obj);
    },
    async moreText(state, index) {
      state.commit("more", index);
    },
    async lessText(state, index) {
      state.commit("less", index);
    },
    async doneTask(state) {
      state.commit("done");
    },
    async notDoneTask(state) {
      state.commit("notDone:");
    },
    async assignTask(state, obj) {
      state.commit("taskAssigned", obj);
    },
    async taskDelete(state, index) {
      state.commit("taskDeleted", index);
    },
    async taskEdit(state, index) {
      state.commit("editTask", index);
    },
    async taskUpdate(state, task) {
      state.commit("updateTask", task);
    },
    async authneticateUser(state, status) {
      state.commit("setAuthentication", status);
    },
    async userRegistration(state, status) {
      state.commit("userRegistered", status);
    },
  },
  getters: {
    lists: (state) => state.nameList,
    person: (state) => state.person,
    personAdded: (state) => state.personAdded,
    edit: (state) => state.editIndex,
    admin: (state) => state.admin,
    register: (state) => state.userRegister,
  },
});
